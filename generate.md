# Generate

## Generate password hash

### One way

```
openssl passwd -1 -salt [salt] [password]

openssl passwd -1 -salt mysalt Raviraj@1234
```

### Second way

```
mkpasswd -m sha-512 newpasswordhere
```



