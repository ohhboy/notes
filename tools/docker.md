# Docker

## Docker

Check available Images

```
sudo docker images
```

Check the running containers

```
sudo docker ps
```

Run the image

```
sudo docker run -it jasonchaffee/kali-linux
```

Commit the Container

```
sudo docker commit CONTAINER_ID NEW_IMAGE_NAME
```

Mount the Directory

```
docker run -d -it --mount type=bind,source="/home/ohhboy/",target=/app jasonchaffee/kali-linux
```

Run kali

```
docker run -d -it jasonchaffee/kali-linux
```



