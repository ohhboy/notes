# Oneliners

### Kali Docker

```
docker run --tty --interactive --mount type=bind,source="/home/ohhboy/",target=/app jasonchaffee/kali-linux /bin/bash
```



### Simple Python Server

```
python -m SimpleHTTPServer 8000

sudo python3 -m http.server 80
```

### Extract tar file

```
tar -zxvf file.tar.gz
```

### Capture Traffic / Man in the middle

```
sudo tcpdump -i tun0 icmp
```

### Install deb file

```
sudo sudo dpkg -i package_file.deb
```



